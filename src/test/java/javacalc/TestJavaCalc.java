package javacalc;
import javacalc.JavaCalc;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestJavaCalc {

	@Test
	public void test1() {
		int result = JavaCalc.add(1, 2);
	    assertEquals(3, result);
	}
	
	@Test
	public void test2() {
		int result = JavaCalc.multiply(1, 2);
	    assertEquals(2, result);
	}
	
	@Test
	public void test3() {
		int result = JavaCalc.subtract(2, 1);
	    assertEquals(1, result);
	}
	
	@Test
	public void test4() {
		int result = JavaCalc.division(2, 2);
	    assertEquals(1, result);
	}

}